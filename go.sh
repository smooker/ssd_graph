#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
cd "$DIR"

rm -f test1 test col1.txt col2.txt asdf.img test1 test
stdbuf -i0 -o0 -e0 dd if=/dev/zero of=./asdf.img bs=1MiB count=4096 iflag=fullblock oflag=dsync status=progress  >>./test1 2>&1
cat test1 | sed -e "s/\r/\n/g" | sed -e "s/,/./g" > test
cat test  | grep bytes | cut -f1 -d" " > col1.txt
cat test  | grep bytes | cut -f8 -d" " > col2.txt
paste col2.txt col1.txt > data.txt
rm col1.txt col2.txt asdf.img test test1
cat ./gnuplot.conf.template | sed -e "s/speed_graph/$1/" | sed -e "s/vgzvgz/$1/" > ./gnuplot.conf
gnuplot ./gnuplot.conf
rm gnuplot.conf
